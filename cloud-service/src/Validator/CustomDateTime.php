<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CustomDateTime extends Constraint
{
	public $message = 'The string "{{ string }}" is not valid datetime format. Valid format is Y-m-d H:i:s';
}
