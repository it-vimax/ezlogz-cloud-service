<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class CustomDateTimeValidator extends ConstraintValidator
{
	public function validate($value, Constraint $constraint)
	{
		if (!$constraint instanceof CustomDateTime) {
		    throw new UnexpectedTypeException($constraint, CustomDateTime::class);
		}
		
		if ($value === null || $value === '') {
		    return;
		}
		
		if (\DateTime::createFromFormat('Y-m-d H:i:s', $value) !== false
			|| \DateTime::createFromFormat('Y-m-d H:i', $value) !== false
			|| \DateTime::createFromFormat('Y-m-d H', $value) !== false
			|| \DateTime::createFromFormat('Y-m-d', $value) !== false
			|| \DateTime::createFromFormat('Y-m', $value) !== false
			|| \DateTime::createFromFormat('Y', $value) !== false) {
			return;
		}
		
		$this->context->buildViolation($constraint->message)
			->setParameter('{{ string }}', $value)
			->addViolation();
	}
}
