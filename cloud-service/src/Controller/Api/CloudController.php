<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Cloud\UseCase\ScannerData;

/**
 * @Route("/api/v1/cloud", name="api.driving")
 * Class DrivingController
 * @package App\Controller\Api
 */
class CloudController extends BaseController
{
	/**
	 * @Route("/driving", name=".driving")
	 */
	public function driving(Request $request, ScannerData\FindDriving\Handler $handler): Response
	{
		$command = $this->serializer->deserialize(json_encode($request->query->all()) ?: "{}", ScannerData\FindDriving\Command::class, 'json');
		$violations = $this->validator->validate($command);
		if (\count($violations)) {
			$json = $this->serializer->serialize($violations, 'json');
			return new JsonResponse($json, 400, [], true);
		}
		
		try {
			$list = $handler->handle($command);
			
			return new JsonResponse(
				$this->serializer->serialize($list, 'json'),
				200, [], true);
		} catch (\DomainException $e) {
			$this->errors->handle($e);
			return new JsonResponse($this->serializer->serialize([
				"title"=> "Domain exception",
				'detail' => $e->getMessage(),
			], 'json'), 400, [], true);
		}
	}
	
	/**
	 * @Route("/connect-disconnect", name=".connect_disconnect")
	 */
	public function connectDisconnect(Request $request, ScannerData\FindConnectDisconnect\Handler $handler): Response
	{
		$command = $this->serializer->deserialize(json_encode($request->query->all()) ?: "{}", ScannerData\FindConnectDisconnect\Command::class, 'json');
		$violations = $this->validator->validate($command);
		if (\count($violations)) {
			$json = $this->serializer->serialize($violations, 'json');
			return new JsonResponse($json, 400, [], true);
		}
		
		try {
			$list = $handler->handle($command);
			
			return new JsonResponse(
				$this->serializer->serialize($list, 'json'),
				200, [], true);
		} catch (\DomainException $e) {
			$this->errors->handle($e);
			return new JsonResponse($this->serializer->serialize([
				"title"=> "Domain exception",
				'detail' => $e->getMessage(),
			], 'json'), 400, [], true);
		}
	}
	
	/**
	 * @Route("/engine-status", name=".engine_status")
	 */
	public function engineStatus(Request $request, ScannerData\FindEngineStatus\OnOffFilter\Handler $handler): Response
	{
		$command = $this->serializer->deserialize(json_encode($request->query->all()) ?: "{}", ScannerData\FindEngineStatus\OnOffFilter\Command::class, 'json');
		$violations = $this->validator->validate($command);
		if (\count($violations)) {
			$json = $this->serializer->serialize($violations, 'json');
			return new JsonResponse($json, 400, [], true);
		}
		
		try {
			$list = $handler->handle($command);
			
			return new JsonResponse(
				$this->serializer->serialize($list, 'json'),
				200, [], true);
		} catch (\DomainException $e) {
			$this->errors->handle($e);
			return new JsonResponse($this->serializer->serialize([
				"title"=> "Domain exception",
				'detail' => $e->getMessage(),
			], 'json'), 400, [], true);
		}
	}
	
	/**
	 * @Route("/motion-status", name=".motion_status")
	 */
	public function motionStatus(Request $request, ScannerData\FindMotionStatus\Handler $handler): Response
	{
		$command = $this->serializer->deserialize(json_encode($request->query->all()) ?: "{}", ScannerData\FindMotionStatus\Command::class, 'json');
		$violations = $this->validator->validate($command);
		if (\count($violations)) {
			$json = $this->serializer->serialize($violations, 'json');
			return new JsonResponse($json, 400, [], true);
		}
		
		try {
			$list = $handler->handle($command);
			
			return new JsonResponse(
				$this->serializer->serialize($list, 'json'),
				200, [], true);
		} catch (\DomainException $e) {
			$this->errors->handle($e);
			return new JsonResponse($this->serializer->serialize([
				"title"=> "Domain exception",
				'detail' => $e->getMessage(),
			], 'json'), 400, [], true);
		}
	}
}
