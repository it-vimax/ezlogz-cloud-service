<?php

namespace App\Controller\Api;

use App\Controller\ErrorHandler;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseController
{
	/**
	 * @var SerializerInterface
	 */
	protected $serializer;
	/**
	 * @var ValidatorInterface
	 */
	protected $validator;
	/**
	 * @var ErrorHandler
	 */
	protected $errors;
	
	/**
	 * BaseController constructor.
	 * @param SerializerInterface $serializer
	 * @param ValidatorInterface $validator
	 * @param ErrorHandler $errors
	 */
	public function __construct(SerializerInterface $serializer, ValidatorInterface $validator, ErrorHandler $errors)
	{
		$this->serializer = $serializer;
		$this->validator = $validator;
		$this->errors = $errors;
	}
}
