<?php

namespace App\Controller\Api\Ezlogz\SmartDevice;

use App\Controller\Api\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Ezlogz\UseCase\SmartDevice\UndefinedDriving;

/**
 * @Route("/api/v1/ezlogz/smart-device/undefined-driving", name="api.ezlogz.smart_device.undefined_driving")
 * Class UndefinedDrivingController
 * @package App\Controller\Api\Ezlogz\SmartDevice
 */
class UndefinedDrivingController extends BaseController
{
	/**
	 * @Route("/create", name=".create", methods={"POST"})
	 * @param Request $request
	 * @param UndefinedDriving\Create\Handler $handler
	 */
	public function create(Request $request, UndefinedDriving\Create\Handler $handler): Response
	{
		/** @var UndefinedDriving\Create\Command $command */
		$command = $this->serializer->deserialize($request->getContent() ?: '{}', UndefinedDriving\Create\Command::class, 'json');
		
		$violations = $this->validator->validate($command);
		if (count($violations)) {
			$json = $this->serializer->serialize($violations, 'json');
			return new JsonResponse($json, 400, [], true);
		}
		
		try {
		    $disconnectDriving = $handler->handler($command);
			return new JsonResponse($this->serializer->serialize($disconnectDriving, 'json'), 201, [], true);
		} catch (\DomainException $exception) {
		    $this->errors->handle($exception);
			return new JsonResponse($this->serializer->serialize([
				'title' => 'Domain exception',
				'detail' => $exception->getMessage(),
			], 'json'), 400, [], true);
		}
	}
}
