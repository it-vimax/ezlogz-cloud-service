<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;

class ErrorHandler
{
	/** @var LoggerInterface */
	private $logger;
	
	public function __construct(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}
	
	public function handle(\DomainException $exception): void
	{
	    $this->logger->warning($exception, ['exception' => $exception]);
	}
}
