<?php

namespace App\Helper;

use App\Model\Cloud\Entity\ScannerData;

class MotionHelper
{
	/**
	 * @param ScannerData[] $list
	 * @return array
	 */
	public static function motionStatusFilter(array $list): array
	{
		$result = [];
		
		if (!empty($list)) {
		    foreach ($list as $scannerData) {
		    	if ($scannerData->MSIsOnPeriodic()) {
		    	    $result[] = $scannerData;
		    	    continue;
		    	}
		    }
		}
		
		return $result;
	}
	
	/**
	 * @param ScannerData[] $list
	 * @return array
	 */
	public static function disconnectStatusFilter(array $list): array
	{
		$result = [];
		
		if (!empty($list)) {
			$isDeviceDisconnStart = false;
			$oneDeviceDisconnect = [];
		    foreach ($list as $scannerData) {
		        if ($scannerData->isDeviceDisconnect()) {
		            if (!$isDeviceDisconnStart) {
		                $isDeviceDisconnStart = true;
		                $oneDeviceDisconnect['start'] = $scannerData;
		                continue;
		            }
		        } elseif ($scannerData->isDeviceConnect()) {
		            if ($isDeviceDisconnStart) {
		                $isDeviceDisconnStart = false;
		                $oneDeviceDisconnect['finish'] = $scannerData;
		                $result[] = $oneDeviceDisconnect;
						$oneDeviceDisconnect = [];
						continue;
		            }
		        }
		    }
		}
		
		return $result;
	}
}
