<?php

namespace App\Helper;

use App\Model\Cloud\Entity\ScannerData;

class EngineHelper
{
	/**
	 * @param ScannerData[] $list
	 */
	public static function onOffFilter(array $list): array
	{
		$result = [];
		
		if (!empty($list)) {
			foreach ($list as $scannerData) {
				if ($scannerData->isEngineOn()) {
					$result[] = $scannerData;
					continue;
				}
				
				if($scannerData->isEngineOff()) {
					$result[] = $scannerData;
					continue;
				}
			}
		}
		
		return $result;
	}
}
