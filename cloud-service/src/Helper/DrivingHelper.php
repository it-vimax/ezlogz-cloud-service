<?php

namespace App\Helper;

use App\Model\Cloud\Entity\ScannerData;
use App\Model\Cloud\Entity\ScannerDataRepository;

class DrivingHelper
{
	/**
	 * @param ScannerData[] $list
	 */
	public static function activeDrivingPeriodFilter(array $list): array
	{
		$result = [];
		
		if (!empty($list)) {
			$isDrivingStart = false;
			$oneDriving = [];
			foreach ($list as $scannerData) {
				if ($scannerData->isDriving()) {
					if (!$isDrivingStart) {
						$isDrivingStart = true;
						$oneDriving['start'] = $scannerData;
					}
				} else {
					if ($isDrivingStart) {
						$isDrivingStart = false;
						$oneDriving['finish'] = $scannerData;
						$result[] = $oneDriving;
						$oneDriving = [];
					}
				}
			}
		}
		
		return $result;
	}
	
	public static function findActiveDrivingFromDisconnect(
		array $disconnectScannerDataList,
		ScannerDataRepository $scannerDataRepo
	): array
	{
		$result = [];
		if (!empty($disconnectScannerDataList)) {
			foreach ($disconnectScannerDataList as $disconnectItem) {
				if (isset($disconnectItem['start']) && isset($disconnectItem['finish'])) {
					$scannerDataListFromDisconnect = $scannerDataRepo->getListWithEventTimeInterval(
						$disconnectItem['start']->getDateTimeUTCTime(),
						$disconnectItem['finish']->getDateTimeUTCTime()
					);
					
					$activeDriving = DrivingHelper::activeDrivingPeriodFilter($scannerDataListFromDisconnect);
					if (!empty($activeDriving)) {
						$result += $activeDriving;
					}
				}
			}
		}
		
		return $result;
	}
}
