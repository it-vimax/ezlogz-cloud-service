<?php

namespace App\Helper;

class CloudHelper
{
	private const TABLE_SCANNER_DATA = 'scanner_data';
	
	public static function generateTableNameByScannerData($scannerId): string
	{
		return mb_strtolower(self::TABLE_SCANNER_DATA . '_' . $scannerId);
	}
}
