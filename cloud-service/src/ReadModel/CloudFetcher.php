<?php

namespace App\ReadModel;

use Doctrine\DBAL\Connection;

class CloudFetcher
{
	private $connection;
	
	/**
	 * ScannerDataFetcher constructor.
	 * @param Connection $connection
	 */
	public function __construct(Connection $connection)
	{
		$this->connection = $connection;
	}
	
	public function isTableExist(string $tableName): bool
	{
		$result = $this->connection->createQueryBuilder()
			->select("EXISTS(
				SELECT * FROM information_schema.tables
					where table_name = '$tableName'
           )")
			->execute()->fetch();
		
		return $result['exists'];
	}
}
