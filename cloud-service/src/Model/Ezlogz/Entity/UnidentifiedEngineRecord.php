<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="UnidentifiedEngineRecord")
 */
class UnidentifiedEngineRecord
{
	/**
	 * @var int
	 * @ORM\Column(name="id")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	/**
	 * @var int
	 * @ORM\Column(name="unidentifiedId")
	 */
	private $unidentifiedId;
	/**
	 * @var int
	 * @ORM\Column(name="date")
	 */
	private $date;
	/**
	 * @var float
	 * @ORM\Column(name="engineHours")
	 */
	private $engineHours;
	/**
	 * @var float
	 * @ORM\Column(name="odometer")
	 */
	private $odometer;
	/**
	 * @var string
	 * @ORM\Column(name="latitude")
	 */
	private $latitude;
	/**
	 * @var string
	 * @ORM\Column(name="longitude")
	 */
	private $longitude;
	/**
	 * @var int
	 * @ORM\Column(name="engineState")
	 */
	private $engineState;
	
	/**
	 * UnidentifiedEngineRecord constructor.
	 * @param int $unidentifiedId
	 * @param int $date
	 * @param float $engineHours
	 * @param float $odometer
	 * @param float $latitude
	 * @param float $longitude
	 * @param int $engineState
	 */
	public function __construct(
		int $unidentifiedId,
		int $date,
		float $engineHours,
		float $odometer,
		float $latitude,
		float $longitude,
		int $engineState
	)
	{
		$this->unidentifiedId = $unidentifiedId;
		$this->date = $date;
		$this->engineHours = $engineHours;
		$this->odometer = $odometer;
		$this->latitude = $latitude;
		$this->longitude = $longitude;
		$this->engineState = $engineState;
	}
}
