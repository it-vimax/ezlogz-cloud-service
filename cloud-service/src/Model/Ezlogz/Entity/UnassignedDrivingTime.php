<?php

namespace App\Model\Ezlogz\Entity;

use App\Model\Cloud\Entity\ScannerData;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="unassigned_driving_time")
 */
class UnassignedDrivingTime
{
	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\Column(name="id")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	/**
	 * @var int
	 * @ORM\Column(name="carrierId")
	 */
	private $carrierId;
	/**
	 * @var int
	 * @ORM\Column(name="truckId")
	 */
	private $truckId;
	/**
	 * @var int
	 * @ORM\Column(name="deviceId")
	 */
	private $deviceId;
	/**
	 * @var int
	 * @ORM\Column(name="dateStart")
	 */
	private $dateStart;
	/**
	 * @var int
	 * @ORM\Column(name="dateEnd")
	 */
	private $dateEnd;
	/**
	 * @var float
	 * @ORM\Column(name="odometerStart")
	 */
	private $odometerStart;
	/**
	 * @var float
	 * @ORM\Column(name="odometerEnd")
	 */
	private $odometerEnd;
	/**
	 * @var string
	 * @ORM\Column(name="dateStartDt")
	 */
	private $dateStartDt;
	/**
	 * @var string
	 * @ORM\Column(name="dateEndDt")
	 */
	private $dateEndDt;
	/**
	 * @var string
	 * @ORM\Column(name="latStart")
	 */
	private $latStart;
	/**
	 * @var string
	 * @ORM\Column(name="longStart")
	 */
	private $longStart;
	/**
	 * @var string
	 * @ORM\Column(name="locationNameStart")
	 */
	private $locationNameStart;
	/**
	 * @var string
	 * @ORM\Column(name="latEnd")
	 */
	private $latEnd;
	/**
	 * @var string
	 * @ORM\Column(name="longEnd")
	 */
	private $longEnd;
	/**
	 * @var string
	 * @ORM\Column(name="locationNameEnd")
	 */
	private $locationNameEnd;
	/**
	 * @var string
	 * @ORM\Column(name="locationIndicatorStart")
	 */
	private $locationIndicatorStart;
	/**
	 * @var string
	 * @ORM\Column(name="locationIndicatorEnd")
	 */
	private $locationIndicatorEnd;
	/**
	 * @var int
	 * @ORM\Column(name="whileMalfunctionStart")
	 */
	private $whileMalfunctionStart;
	/**
	 * @var int
	 * @ORM\Column(name="whileMalfunctionEnd")
	 */
	private $whileMalfunctionEnd;
	/**
	 * @var int
	 * @ORM\Column(name="distanceSinceLastCoordStart")
	 */
	private $distanceSinceLastCoordStart;
	/**
	 * @var int
	 * @ORM\Column(name="distanceSinceLastCoordEnd")
	 */
	private $distanceSinceLastCoordEnd;
	/**
	 * @var string
	 * @ORM\Column(name="dateTimeUtc")
	 */
	private $dateTimeUtc;
	/**
	 * @var string
	 * @ORM\Column(name="assigned_at")
	 */
	private $assignedAt;
	
	/**
	 * UnassignedDrivingTime constructor.
	 * @param int $carrierId
	 * @param int $truckId
	 * @param int $deviceId
	 * @param int $dateStart
	 * @param int $dateEnd
	 */
	public function __construct(int $carrierId, int $truckId, int $deviceId, int $dateStart, int $dateEnd)
	{
		$this->carrierId = $carrierId;
		$this->truckId = $truckId;
		$this->deviceId = $deviceId;
		$this->dateStart = $dateStart;
		$this->dateEnd = $dateEnd;
	}
	
	public static function createFromCloud(
		int $carrierId,
		int $truckId,
		int $deviceId,
		\DateTimeImmutable $dateStart,
		\DateTimeImmutable $dateEnd,
		ScannerData $scannerDataStart,
		ScannerData $scannerDataFinish
	)
	{
		$unassignedDrivingTime = new self(
			$carrierId,
			$truckId,
			$deviceId,
			$dateStart->getTimestamp(),
			$dateEnd->getTimestamp()
		);
		
		$unassignedDrivingTime->setOdometerStart($scannerDataStart->getOdomenter());
		$unassignedDrivingTime->setOdometerEnd($scannerDataFinish->getOdomenter());
		
		$unassignedDrivingTime->setDateStartDt($scannerDataStart->getDateTimeUTCTime()->format('Y-m-d H:i:s'));
		$unassignedDrivingTime->setDateEndDt($scannerDataFinish->getDateTimeUTCTime()->format('Y-m-d H:i:s'));
		
		$unassignedDrivingTime->setLatStart((string)$scannerDataStart->getLatitudeDegrees());
		$unassignedDrivingTime->setLatEnd((string)$scannerDataFinish->getLatitudeDegrees());
		
		$unassignedDrivingTime->setLongStart((string)$scannerDataStart->getLongitudeDegrees());
		$unassignedDrivingTime->setLongEnd((string)$scannerDataFinish->getLongitudeDegrees());
		
		return $unassignedDrivingTime;
	}
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @return int
	 */
	public function getCarrierId(): int
	{
		return $this->carrierId;
	}
	
	/**
	 * @param int $carrierId
	 */
	public function setcarrierId(int $carrierId): void
	{
		$this->carrierId = $carrierId;
	}
	
	/**
	 * @return int
	 */
	public function getTruckId(): int
	{
		return $this->truckId;
	}
	
	/**
	 * @param int $truckId
	 */
	public function setTruckId(int $truckId): void
	{
		$this->truckId = $truckId;
	}
	
	/**
	 * @return int
	 */
	public function getDeviceId(): int
	{
		return $this->deviceId;
	}
	
	/**
	 * @param int $deviceId
	 */
	public function setDeviceId(int $deviceId): void
	{
		$this->deviceId = $deviceId;
	}
	
	/**
	 * @return int
	 */
	public function getDateStart(): int
	{
		return $this->dateStart;
	}
	
	/**
	 * @param int $dateStart
	 */
	public function setDateStart(int $dateStart): void
	{
		$this->dateStart = $dateStart;
	}
	
	/**
	 * @return int
	 */
	public function getDateEnd(): int
	{
		return $this->dateEnd;
	}
	
	/**
	 * @param int $dateEnd
	 */
	public function setDateEnd(int $dateEnd): void
	{
		$this->dateEnd = $dateEnd;
	}
	
	/**
	 * @return float
	 */
	public function getOdometerStart(): float
	{
		return $this->odometerStart;
	}
	
	/**
	 * @param float $odometerStart
	 */
	public function setOdometerStart(float $odometerStart): void
	{
		$this->odometerStart = $odometerStart;
	}
	
	/**
	 * @return float
	 */
	public function getOdometerEnd(): float
	{
		return $this->odometerEnd;
	}
	
	/**
	 * @param float $odometerEnd
	 */
	public function setOdometerEnd(float $odometerEnd): void
	{
		$this->odometerEnd = $odometerEnd;
	}
	
	/**
	 * @return string
	 */
	public function getDateStartDt(): string
	{
		return $this->dateStartDt;
	}
	
	/**
	 * @param string $dateStartDt
	 */
	public function setDateStartDt(string $dateStartDt): void
	{
		$this->dateStartDt = $dateStartDt;
	}
	
	/**
	 * @return string
	 */
	public function getDateEndDt(): string
	{
		return $this->dateEndDt;
	}
	
	/**
	 * @param string $dateEndDt
	 */
	public function setDateEndDt(string $dateEndDt): void
	{
		$this->dateEndDt = $dateEndDt;
	}
	
	/**
	 * @return string
	 */
	public function getLatStart(): string
	{
		return $this->latStart;
	}
	
	/**
	 * @param string $latStart
	 */
	public function setLatStart(string $latStart): void
	{
		$this->latStart = $latStart;
	}
	
	/**
	 * @return string
	 */
	public function getLongStart(): string
	{
		return $this->longStart;
	}
	
	/**
	 * @param string $longStart
	 */
	public function setLongStart(string $longStart): void
	{
		$this->longStart = $longStart;
	}
	
	/**
	 * @return string
	 */
	public function getLocationNameStart(): string
	{
		return $this->locationNameStart;
	}
	
	/**
	 * @param string $locationNameStart
	 */
	public function setLocationNameStart(string $locationNameStart): void
	{
		$this->locationNameStart = $locationNameStart;
	}
	
	/**
	 * @return string
	 */
	public function getLatEnd(): string
	{
		return $this->latEnd;
	}
	
	/**
	 * @param string $latEnd
	 */
	public function setLatEnd(string $latEnd): void
	{
		$this->latEnd = $latEnd;
	}
	
	/**
	 * @return string
	 */
	public function getLongEnd(): string
	{
		return $this->longEnd;
	}
	
	/**
	 * @param string $longEnd
	 */
	public function setLongEnd(string $longEnd): void
	{
		$this->longEnd = $longEnd;
	}
	
	/**
	 * @return string
	 */
	public function getLocationNameEnd(): string
	{
		return $this->locationNameEnd;
	}
	
	/**
	 * @param string $locationNameEnd
	 */
	public function setLocationNameEnd(string $locationNameEnd): void
	{
		$this->locationNameEnd = $locationNameEnd;
	}
	
	/**
	 * @return string
	 */
	public function getLocationIndicatorStart(): string
	{
		return $this->locationIndicatorStart;
	}
	
	/**
	 * @param string $locationIndicatorStart
	 */
	public function setLocationIndicatorStart(string $locationIndicatorStart): void
	{
		$this->locationIndicatorStart = $locationIndicatorStart;
	}
	
	/**
	 * @return string
	 */
	public function getLocationIndicatorEnd(): string
	{
		return $this->locationIndicatorEnd;
	}
	
	/**
	 * @param string $locationIndicatorEnd
	 */
	public function setLocationIndicatorEnd(string $locationIndicatorEnd): void
	{
		$this->locationIndicatorEnd = $locationIndicatorEnd;
	}
	
	/**
	 * @return int
	 */
	public function getWhileMalfunctionStart(): int
	{
		return $this->whileMalfunctionStart;
	}
	
	/**
	 * @param int $whileMalfunctionStart
	 */
	public function setWhileMalfunctionStart(int $whileMalfunctionStart): void
	{
		$this->whileMalfunctionStart = $whileMalfunctionStart;
	}
	
	/**
	 * @return int
	 */
	public function getWhileMalfunctionEnd(): int
	{
		return $this->whileMalfunctionEnd;
	}
	
	/**
	 * @param int $whileMalfunctionEnd
	 */
	public function setWhileMalfunctionEnd(int $whileMalfunctionEnd): void
	{
		$this->whileMalfunctionEnd = $whileMalfunctionEnd;
	}
	
	/**
	 * @return int
	 */
	public function getDistanceSinceLastCoordStart(): int
	{
		return $this->distanceSinceLastCoordStart;
	}
	
	/**
	 * @param int $distanceSinceLastCoordStart
	 */
	public function setDistanceSinceLastCoordStart(int $distanceSinceLastCoordStart): void
	{
		$this->distanceSinceLastCoordStart = $distanceSinceLastCoordStart;
	}
	
	/**
	 * @return int
	 */
	public function getDistanceSinceLastCoordEnd(): int
	{
		return $this->distanceSinceLastCoordEnd;
	}
	
	/**
	 * @param int $distanceSinceLastCoordEnd
	 */
	public function setDistanceSinceLastCoordEnd(int $distanceSinceLastCoordEnd): void
	{
		$this->distanceSinceLastCoordEnd = $distanceSinceLastCoordEnd;
	}
	
	/**
	 * @return string
	 */
	public function getDateTimeUtc(): string
	{
		return $this->dateTimeUtc;
	}
	
	/**
	 * @param string $dateTimeUtc
	 */
	public function setDateTimeUtc(string $dateTimeUtc): void
	{
		$this->dateTimeUtc = $dateTimeUtc;
	}
	
	/**
	 * @return string
	 */
	public function getAssignedAt(): string
	{
		return $this->assignedAt;
	}
	
	/**
	 * @param string $assignedAt
	 */
	public function setAssignedAt(string $assignedAt): void
	{
		$this->assignedAt = $assignedAt;
	}
}
