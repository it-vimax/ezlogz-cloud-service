<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="eld_scanners")
 * Class ELDScanner
 * @package App\Model\Ezlogz\Entity
 */
class ELDScanner
{
	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(name="id")
	 */
	private $id;
	/**
	 * @var integer
	 * @ORM\Column(name="status")
	 */
	private $status;
	/**
	 * @var integer
	 * @ORM\Column(name="fleet")
	 */
	private $fleet;
	/**
	 * @var integer
	 * @ORM\Column(name="localId")
	 */
	private $localId;
	/**
	 * @var integer
	 * @ORM\Column(name="lastTruck")
	 */
	private $lastTruck;
	/**
	 * @var integer
	 * @ORM\Column(name="lastDriver")
	 */
	private $lastDriver;
	/**
	 * @var integer
	 * @ORM\Column(name="registrationNumber")
	 */
	private $registrationNumber;
	/**
	 * @var string
	 * @ORM\Column(name="BLEAddress")
	 */
	private $BLEAddress;
	/**
	 * @var integer
	 * @ORM\Column(name="demo")
	 */
	private $demo;
	/**
	 * @var integer
	 * @ORM\Column(name="demoDate")
	 */
	private $demoDate;
	/**
	 * @var integer
	 * @ORM\Column(name="demoLastMessage")
	 */
	private $demoLastMessage;
	/**
	 * @var integer
	 * @ORM\Column(name="type")
	 */
	private $type;
	/**
	 * @var integer
	 * @ORM\Column(name="activationDate")
	 */
	private $activationDate;
	/**
	 * @var integer
	 * @ORM\Column(name="userId")
	 */
	private $userId;
	/**
	 * @var integer
	 * @ORM\Column(name="version")
	 */
	private $version;
	/**
	 * @var string
	 * @ORM\Column(name="updateVersion")
	 */
	private $updateVersion;
	/**
	 * @var integer
	 * @ORM\Column(name="paid_till")
	 */
	private $paidTill;
	/**
	 * @var integer
	 * @ORM\Column(name="tariffStart")
	 */
	private $tariffStart;
	/**
	 * @var float
	 * @ORM\Column(name="deposit")
	 */
	private $deposit;
	/**
	 * @var integer
	 * @ORM\Column(name="params")
	 */
	private $params;
	/**
	 * @var integer
	 * @ORM\Column(name="teamId")
	 */
	private $teamId;
	/**
	 * @var integer
	 * @ORM\Column(name="tariffId")
	 */
	private $tariffId;
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @return int
	 */
	public function getStatus(): int
	{
		return $this->status;
	}
	
	/**
	 * @return int
	 */
	public function getCarrierId(): int
	{
		return $this->fleet;
	}
	
	/**
	 * @return int
	 */
	public function getLocalId(): int
	{
		return $this->localId;
	}
	
	/**
	 * @return int
	 */
	public function getLastTruck(): int
	{
		return $this->lastTruck;
	}
	
	/**
	 * @return int
	 */
	public function getLastDriver(): int
	{
		return $this->lastDriver;
	}
	
	/**
	 * @return int
	 */
	public function getRegistrationNumber(): int
	{
		return $this->registrationNumber;
	}
	
	/**
	 * @return string
	 */
	public function getBLEAddress(): string
	{
		return $this->BLEAddress;
	}
	
	/**
	 * @return int
	 */
	public function getDemo(): int
	{
		return $this->demo;
	}
	
	/**
	 * @return int
	 */
	public function getDemoDate(): int
	{
		return $this->demoDate;
	}
	
	/**
	 * @return int
	 */
	public function getDemoLastMessage(): int
	{
		return $this->demoLastMessage;
	}
	
	/**
	 * @return int
	 */
	public function getType(): int
	{
		return $this->type;
	}
	
	/**
	 * @return int
	 */
	public function getActivationDate(): int
	{
		return $this->activationDate;
	}
	
	/**
	 * @return int
	 */
	public function getUserId(): int
	{
		return $this->userId;
	}
	
	/**
	 * @return int
	 */
	public function getVersion(): int
	{
		return $this->version;
	}
	
	/**
	 * @return string
	 */
	public function getUpdateVersion(): string
	{
		return $this->updateVersion;
	}
	
	/**
	 * @return int
	 */
	public function getPaidTill(): int
	{
		return $this->paidTill;
	}
	
	/**
	 * @return int
	 */
	public function getTariffStart(): int
	{
		return $this->tariffStart;
	}
	
	/**
	 * @return float
	 */
	public function getDeposit(): float
	{
		return $this->deposit;
	}
	
	/**
	 * @return int
	 */
	public function getParams(): int
	{
		return $this->params;
	}
	
	/**
	 * @return int
	 */
	public function getTeamId(): int
	{
		return $this->teamId;
	}
	
	/**
	 * @return int
	 */
	public function getTariffId(): int
	{
		return $this->tariffId;
	}
}
