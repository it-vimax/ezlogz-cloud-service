<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\EntityManagerInterface;

class UnidentifiedEngineRecordRepository
{
	private $em;
	private $repo;
	
	/**
	 * ScannerUnidentifiedRecordsRepository constructor.
	 * @param EntityManagerInterface $em
	 */
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(UnidentifiedEngineRecord::class);
	}
	
	public function add(UnidentifiedEngineRecord $model): void
	{
		$this->em->persist($model);
	}
}
