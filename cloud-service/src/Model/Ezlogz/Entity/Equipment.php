<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="equipment")
 * Class TruckELDInfo
 * @package App\Model\Ezlogz\Entity
 */
class Equipment
{
	/**
	 * @var int
	 * @ORM\Column(name="id")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Id
	 */
	private $id;
	/**
	 * @var int
	 * @ORM\Column(name="truckTrailer")
	 */
	private $truckTrailer;
	/**
	 * @var int
	 * @ORM\Column(name="carrierId")
	 */
	private $carrierId;
	/**
	 * @var int
	 * @ORM\Column(name="userId")
	 */
	private $userId;
	/**
	 * @var string
	 * @ORM\Column(name="Name")
	 */
	private $name;
	/**
	 * @var string
	 * @ORM\Column(name="Owner")
	 */
	private $owner;
	/**
	 * @var int
	 * @ORM\Column(name="Year")
	 */
	private $year;
	/**
	 * @var int
	 * @ORM\Column(name="Type")
	 */
	private $type;
	/**
	 * @var string
	 * @ORM\Column(name="TireSize")
	 */
	private $tireSize;
	/**
	 * @var string
	 * @ORM\Column(name="Length")
	 */
	private $length;
	/**
	 * @var string
	 * @ORM\Column(name="Fuel")
	 */
	private $fuel;
	/**
	 * @var string
	 * @ORM\Column(name="Axel")
	 */
	private $axel;
	/**
	 * @var string
	 * @ORM\Column(name="Color")
	 */
	private $color;
	/**
	 * @var string
	 * @ORM\Column(name="Make")
	 */
	private $make;
	/**
	 * @var string
	 * @ORM\Column(name="Model")
	 */
	private $model;
	/**
	 * @var string
	 * @ORM\Column(name="VIN")
	 */
	private $vin;
	/**
	 * @var string
	 * @ORM\Column(name="GrossWeight")
	 */
	private $grossWeight;
	/**
	 * @var string
	 * @ORM\Column(name="UnlandWeight")
	 */
	private $unlandWeight;
	/**
	 * @var string
	 * @ORM\Column(name="Plate")
	 */
	private $plate;
	/**
	 * @var string
	 * @ORM\Column(name="State")
	 */
	private $state;
	/**
	 * @var string
	 * @ORM\Column(name="NYCert")
	 */
	private $NYCert;
	/**
	 * @var string
	 * @ORM\Column(name="InspectionDue")
	 */
	private $inspectionDue;
	/**
	 * @var string
	 * @ORM\Column(name="90DayExp")
	 */
	private $ninetyDayExp;
	/**
	 * @var string
	 * @ORM\Column(name="ProRateExp")
	 */
	private $proRateExp;
	/**
	 * @var string
	 * @ORM\Column(name="ExpDate")
	 */
	private $expDate;
	/**
	 * @var int
	 * @ORM\Column(name="isActive")
	 */
	private $isActive;
	/**
	 * @var string
	 * @ORM\Column(name="LastDispatch")
	 */
	private $lastDispatch;
	/**
	 * @var string
	 * @ORM\Column(name="Notes")
	 */
	private $notes;
	/**
	 * @var int
	 * @ORM\Column(name="straight")
	 */
	private $straight;
	/**
	 * @var float
	 * @ORM\Column(name="lat")
	 */
	private $lat;
	/**
	 * @var float
	 * @ORM\Column(name="lng")
	 */
	private $lng;
	/**
	 * @var int
	 * @ORM\Column(name="locTime")
	 */
	private $locTime;
	/**
	 * @var TruckELDInfo
	 * @ORM\OneToOne(targetEntity="TruckELDInfo")
	 * @ORM\JoinColumn(name="id", referencedColumnName="equipmentId")
	 */
	private $truckELDInfo;
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @return int
	 */
	public function getTruckTrailer(): int
	{
		return $this->truckTrailer;
	}
	
	/**
	 * @return int
	 */
	public function getCarrierId(): int
	{
		return $this->carrierId;
	}
	
	/**
	 * @return int
	 */
	public function getUserId(): int
	{
		return $this->userId;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}
	
	/**
	 * @return string
	 */
	public function getOwner(): string
	{
		return $this->owner;
	}
	
	/**
	 * @return int
	 */
	public function getYear(): int
	{
		return $this->year;
	}
	
	/**
	 * @return int
	 */
	public function getType(): int
	{
		return $this->type;
	}
	
	/**
	 * @return string
	 */
	public function getTireSize(): string
	{
		return $this->tireSize;
	}
	
	/**
	 * @return string
	 */
	public function getLength(): string
	{
		return $this->length;
	}
	
	/**
	 * @return string
	 */
	public function getFuel(): string
	{
		return $this->fuel;
	}
	
	/**
	 * @return string
	 */
	public function getAxel(): string
	{
		return $this->axel;
	}
	
	/**
	 * @return string
	 */
	public function getColor(): string
	{
		return $this->color;
	}
	
	/**
	 * @return string
	 */
	public function getMake(): string
	{
		return $this->make;
	}
	
	/**
	 * @return string
	 */
	public function getModel(): string
	{
		return $this->model;
	}
	
	/**
	 * @return string
	 */
	public function getVin(): string
	{
		return $this->vin;
	}
	
	/**
	 * @return string
	 */
	public function getGrossWeight(): string
	{
		return $this->grossWeight;
	}
	
	/**
	 * @return string
	 */
	public function getUnlandWeight(): string
	{
		return $this->unlandWeight;
	}
	
	/**
	 * @return string
	 */
	public function getPlate(): string
	{
		return $this->plate;
	}
	
	/**
	 * @return string
	 */
	public function getState(): string
	{
		return $this->state;
	}
	
	/**
	 * @return string
	 */
	public function getNYCert(): string
	{
		return $this->NYCert;
	}
	
	/**
	 * @return string
	 */
	public function getInspectionDue(): string
	{
		return $this->inspectionDue;
	}
	
	/**
	 * @return string
	 */
	public function getNinetyDayExp(): string
	{
		return $this->ninetyDayExp;
	}
	
	/**
	 * @return string
	 */
	public function getProRateExp(): string
	{
		return $this->proRateExp;
	}
	
	/**
	 * @return string
	 */
	public function getExpDate(): string
	{
		return $this->expDate;
	}
	
	/**
	 * @return int
	 */
	public function getIsActive(): int
	{
		return $this->isActive;
	}
	
	/**
	 * @return string
	 */
	public function getLastDispatch(): string
	{
		return $this->lastDispatch;
	}
	
	/**
	 * @return string
	 */
	public function getNotes(): string
	{
		return $this->notes;
	}
	
	/**
	 * @return int
	 */
	public function getStraight(): int
	{
		return $this->straight;
	}
	
	/**
	 * @return float
	 */
	public function getLat(): float
	{
		return $this->lat;
	}
	
	/**
	 * @return float
	 */
	public function getLng(): float
	{
		return $this->lng;
	}
	
	/**
	 * @return int
	 */
	public function getLocTime(): int
	{
		return $this->locTime;
	}
}
