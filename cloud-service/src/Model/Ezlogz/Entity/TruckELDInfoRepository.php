<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;

class TruckELDInfoRepository
{
	private $em;
	private $repo;
	
	/**
	 * ScannerUnidentifiedRecordsRepository constructor.
	 * @param EntityManagerInterface $em
	 */
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(TruckELDInfo::class);
	}
	
	public function getByVin($vin): TruckELDInfo
	{
		if (!$model = $this->repo->findOneBy(['vin' => $vin])) {
			throw new \DomainException('TruckELDInfo is not exist by vin ' . $vin);
		}
		
		return $model;
	}
	
	public function fetchByVinAndCarrierId($vin, $carrierId): ?TruckELDInfo
	{
		$model = $this->repo->createQueryBuilder('t')
			->select('t')
			->join(Equipment::class, 'e', Join::WITH, 't.equipment = e.id')
			->andWhere('t.vin = :vin')
			->andWhere('e.carrierId = :carrierId')
			->setParameter('vin', $vin)
			->setParameter('carrierId', $carrierId)
			->getQuery()->getOneOrNullResult();
			
		return $model;
	}
	
	public function add(TruckELDInfo $model): void
	{
		$this->em->persist($model);
	}
}
