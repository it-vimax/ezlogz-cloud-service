<?php

namespace App\Model\Ezlogz\Entity;

use App\Helper\MotionHelper;
use App\Model\Cloud\Entity\ScannerData;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="UnidentifiedPeriodicRecord")
 */
class UnidentifiedPeriodicRecord
{
	/**
	 * @var int
	 * @ORM\Column(name="id")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	/**
	 * @var int
	 * @ORM\Column(name="unidentifiedId")
	 */
	private $unidentifiedId;
	/**
	 * @var int
	 * @ORM\Column(name="date")
	 */
	private $date;
	/**
	 * @var float
	 * @ORM\Column(name="engineHours")
	 */
	private $engineHours;
	/**
	 * @var float
	 * @ORM\Column(name="odometer")
	 */
	private $odometer;
	/**
	 * @var string
	 * @ORM\Column(name="latitude")
	 */
	private $latitude;
	/**
	 * @var string
	 * @ORM\Column(name="longitude")
	 */
	private $longitude;
	
	/**
	 * UnidentifiedPeriodicRecord constructor.
	 * @param int $unidentifiedId
	 * @param int $date
	 * @param float $engineHours
	 * @param float $odometer
	 * @param string $latitude
	 * @param string $longitude
	 */
	public function __construct(int $unidentifiedId, int $date, float $engineHours, float $odometer, string $latitude, string $longitude)
	{
		$this->unidentifiedId = $unidentifiedId;
		$this->date = $date;
		$this->engineHours = $engineHours;
		$this->odometer = $odometer;
		$this->latitude = $latitude;
		$this->longitude = $longitude;
	}
	
	/**
	 * @param int $drivingId
	 * @param $scannerDataListByDriving
	 * @param UnidentifiedPeriodicRecordRepository $repo
	 */
	public static function createUnidentifiedPeriodicRecords(
		int $drivingId,
		$scannerDataListByDriving,
		UnidentifiedPeriodicRecordRepository $repo
	)
	{
		$onPeriodicList = MotionHelper::motionStatusFilter($scannerDataListByDriving);
		if (!empty($onPeriodicList)) {
			/** @var ScannerData $onPeriodic */
			foreach ($onPeriodicList as $onPeriodic) {
				$unidentifiedPeriodicRecord = new self(
					$drivingId,
					$onPeriodic->getEventUtcTime(),
					$onPeriodic->getEngineHours(),
					$onPeriodic->getOdomenter(),
					$onPeriodic->getLatitudeDegrees(),
					$onPeriodic->getLongitudeDegrees()
				);
				
				$repo->add($unidentifiedPeriodicRecord);
			}
		}
	}
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @return int
	 */
	public function getUnidentifiedId(): int
	{
		return $this->unidentifiedId;
	}
	
	/**
	 * @return int
	 */
	public function getDate(): int
	{
		return $this->date;
	}
	
	/**
	 * @return float
	 */
	public function getEngineHours(): float
	{
		return $this->engineHours;
	}
	
	/**
	 * @return float
	 */
	public function getOdometer(): float
	{
		return $this->odometer;
	}
	
	/**
	 * @return string
	 */
	public function getLatitude(): string
	{
		return $this->latitude;
	}
	
	/**
	 * @return string
	 */
	public function getLongitude(): string
	{
		return $this->longitude;
	}
}
