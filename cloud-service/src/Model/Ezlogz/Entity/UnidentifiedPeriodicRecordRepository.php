<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\EntityManagerInterface;

class UnidentifiedPeriodicRecordRepository
{
	private $em;
	private $repo;
	
	/**
	 * ScannerUnidentifiedRecordsRepository constructor.
	 * @param EntityManagerInterface $em
	 */
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(UnidentifiedPeriodicRecord::class);
	}
	
	public function add(UnidentifiedPeriodicRecord $model): void
	{
		$this->em->persist($model);
	}
}
