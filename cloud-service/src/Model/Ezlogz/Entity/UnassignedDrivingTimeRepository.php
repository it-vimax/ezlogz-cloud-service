<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\EntityManagerInterface;

class UnassignedDrivingTimeRepository
{
	private $em;
	private $repo;
	
	/**
	 * ScannerUnidentifiedRecordsRepository constructor.
	 * @param EntityManagerInterface $em
	 */
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(UnassignedDrivingTime::class);
	}
	
	public function add(UnassignedDrivingTime $model): void
	{
		$this->em->persist($model);
	}
}
