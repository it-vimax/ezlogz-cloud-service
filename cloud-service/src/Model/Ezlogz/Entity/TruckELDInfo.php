<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="truckeldinfo")
 * Class TruckELDInfo
 * @package App\Model\Ezlogz\Entity
 */
class TruckELDInfo
{
	/**
	 * @var int
	 * @ORM\Column(name="equipmentId")
	 */
	private $equipmentId;
	/**
	 * @var float
	 * @ORM\Column(name="odometer")
	 */
	private $odometer;
	/**
	 * @var float
	 * @ORM\Column(name="curOdometer")
	 */
	private $curOdometer;
	/**
	 * @var float
	 * @ORM\Column(name="curEngineHours")
	 */
	private $curEngineHours;
	/**
	 * @var string
	 * @ORM\Column(name="vin")
	 */
	private $vin;
	/**
	 * @var string
	 * @ORM\Column(name="protocol")
	 */
	private $protocol;
	/**
	 * @var string
	 * @ORM\Column(name="bluetoothId")
	 */
	private $bluetoothId;
	/**
	 * @var int
	 * @ORM\Column(name="unique_id")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Id
	 */
	private $uniqueId;
	/**
	 * @var Equipment
	 * @ORM\OneToOne(targetEntity="Equipment")
	 * @ORM\JoinColumn(name="equipmentId", referencedColumnName="id")
	 */
	private $equipment;
	
	/**
	 * @return int
	 */
	public function getEquipmentId(): int
	{
		return $this->equipmentId;
	}
	
	/**
	 * @return float
	 */
	public function getOdometer(): float
	{
		return $this->odometer;
	}
	
	/**
	 * @return float
	 */
	public function getCurOdometer(): float
	{
		return $this->curOdometer;
	}
	
	/**
	 * @return float
	 */
	public function getCurEngineHours(): float
	{
		return $this->curEngineHours;
	}
	
	/**
	 * @return string
	 */
	public function getVin(): string
	{
		return $this->vin;
	}
	
	/**
	 * @return string
	 */
	public function getProtocol(): string
	{
		return $this->protocol;
	}
	
	/**
	 * @return string
	 */
	public function getBluetoothId(): string
	{
		return $this->bluetoothId;
	}
	
	/**
	 * @return int
	 */
	public function getUniqueId(): int
	{
		return $this->uniqueId;
	}
}
