<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\EntityManagerInterface;

class EquipmentRepository
{
	private $em;
	private $repo;
	
	/**
	 * ScannerUnidentifiedRecordsRepository constructor.
	 * @param EntityManagerInterface $em
	 */
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(Equipment::class);
	}
	
	public function add(Equipment $model): void
	{
		$this->em->persist($model);
	}
}
