<?php

namespace App\Model\Ezlogz\Entity;

use App\Model\Cloud\Entity\ScannerData;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="scannerunidentifiedrecords")
 */
class ScannerUnidentifiedRecords
{
	/**
	 * @var int
	 * @ORM\Column(name="scannerId")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $scannerId;
	/**
	 * @var string
	 * @ORM\Column(name="vin")
	 */
	private $vin;
	/**
	 * @var string
	 * @ORM\Column(name="value")
	 */
	private $value;
	/**
	 * @var int
	 * @ORM\Column(name="dateTime")
	 */
	private $dateTime;
	
	/**
	 * ScannerUnidentifiedRecords constructor.
	 * @param int $scannerId
	 * @param string $vin
	 * @param string $value
	 * @param int $dateTime
	 */
	public function __construct(int $scannerId, string $vin, string $value, int $dateTime)
	{
		$this->scannerId = $scannerId;
		$this->vin = $vin;
		$this->value = $value;
		$this->dateTime = $dateTime;
	}
}
