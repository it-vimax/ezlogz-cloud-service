<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\EntityManagerInterface;

class ScannerUnidentifiedRecordsRepository
{
	private $em;
	private $repo;
	
	/**
	 * ScannerUnidentifiedRecordsRepository constructor.
	 * @param EntityManagerInterface $em
	 */
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(ScannerUnidentifiedRecords::class);
	}
	
	public function add(ScannerUnidentifiedRecords $model): void
	{
	    $this->em->persist($model);
	}
}
