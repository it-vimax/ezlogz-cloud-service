<?php

namespace App\Model\Ezlogz\Entity;

use Doctrine\ORM\EntityManagerInterface;

class ELDScannerRepository
{
	private $em;
	private $repo;
	
	/**
	 * ScannerUnidentifiedRecordsRepository constructor.
	 * @param EntityManagerInterface $em
	 */
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(ELDScanner::class);
	}
	
	public function getByMACAddress($mac): ELDScanner
	{
		$model = $this->repo->createQueryBuilder('e')
			->andWhere('e.BLEAddress like :BLEAddress')
			->orderBy('e.id', 'DESC')
			->setMaxResults(1)
			->setParameter('BLEAddress', '%' . $mac)
			->getQuery()
			->getOneOrNullResult();
		
	    if (!$model) {
	        throw new \DomainException('Smart ELDScanner is not exist by mac QW-' . $mac);
	    }
	    
	    return $model;
	}
	
	public function add(ELDScanner $model): void
	{
		$this->em->persist($model);
	}
}
