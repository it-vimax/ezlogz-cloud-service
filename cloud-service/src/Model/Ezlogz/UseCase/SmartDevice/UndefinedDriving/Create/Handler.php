<?php

namespace App\Model\Ezlogz\UseCase\SmartDevice\UndefinedDriving\Create;

use App\Helper\CloudHelper;
use App\Helper\DrivingHelper;
use App\Helper\MotionHelper;
use App\Model\Cloud;
use App\Model\Cloud\Entity\ScannerData;
use App\Model\Ezlogz;
use App\Model\EzlogzFlusher;
use App\ReadModel\CloudFetcher;
use Laminas\EventManager\Exception\DomainException;

class Handler
{
	// from Cloud
	/** @var Cloud\Entity\ScannerDataRepository */
	private $scannerDataRepo;
	
	// from Ezlogz
	/** @var Ezlogz\Entity\UnidentifiedEngineRecordRepository */
	private $unidentifiedEngineRecordRepo;
	/** @var Ezlogz\Entity\UnidentifiedPeriodicRecordRepository */
	private $unidentifiedPeriodicRecordRepo;
	/** @var Ezlogz\Entity\UnassignedDrivingTimeRepository */
	private $unassignedDrivingTimeRepo;
	/** @var Ezlogz\Entity\ScannerUnidentifiedRecordsRepository */
	private $scannerUnidentifiedRecordsRepo;
	/** @var Ezlogz\Entity\TruckELDInfoRepository */
	private $truckELDInfoRepo;
	/** @var Ezlogz\Entity\ELDScannerRepository */
	private $ELDScannerRepo;
	
	/** @var EzlogzFlusher */
	private $flusher;
	/** @var CloudFetcher */
	private $cloudFetcher;
	
	/**
	 * Handler constructor.
	 * @param Cloud\Entity\ScannerDataRepository $scannerDataRepo
	 * @param Ezlogz\Entity\UnidentifiedEngineRecordRepository $unidentifiedEngineRecordRepo
	 * @param Ezlogz\Entity\UnidentifiedPeriodicRecordRepository $unidentifiedPeriodicRecordRepo
	 * @param Ezlogz\Entity\UnassignedDrivingTimeRepository $unassignedDrivingTimeRepo
	 * @param Ezlogz\Entity\ScannerUnidentifiedRecordsRepository $scannerUnidentifiedRecordsRepo
	 * @param CloudFetcher $cloudFetcher
	 * @param EzlogzFlusher $flusher
	 */
	public function __construct(
		Cloud\Entity\ScannerDataRepository $scannerDataRepo,
		Ezlogz\Entity\UnidentifiedEngineRecordRepository $unidentifiedEngineRecordRepo,
		Ezlogz\Entity\UnidentifiedPeriodicRecordRepository $unidentifiedPeriodicRecordRepo,
		Ezlogz\Entity\UnassignedDrivingTimeRepository $unassignedDrivingTimeRepo,
		Ezlogz\Entity\ScannerUnidentifiedRecordsRepository $scannerUnidentifiedRecordsRepo,
		Ezlogz\Entity\TruckELDInfoRepository $truckELDInfoRepo,
		Ezlogz\Entity\ELDScannerRepository $ELDScannerRepo,
		CloudFetcher $cloudFetcher,
		EzlogzFlusher $flusher
	)
	{
		$this->scannerDataRepo = $scannerDataRepo;
		$this->unidentifiedEngineRecordRepo = $unidentifiedEngineRecordRepo;
		$this->unidentifiedPeriodicRecordRepo = $unidentifiedPeriodicRecordRepo;
		$this->unassignedDrivingTimeRepo = $unassignedDrivingTimeRepo;
		$this->scannerUnidentifiedRecordsRepo = $scannerUnidentifiedRecordsRepo;
		$this->truckELDInfoRepo = $truckELDInfoRepo;
		$this->ELDScannerRepo = $ELDScannerRepo;
		$this->cloudFetcher = $cloudFetcher;
		$this->flusher = $flusher;
	}
	
	public function handler(Command $command)
	{
		$tableName = CloudHelper::generateTableNameByScannerData($command->deviceMAC);
		
		if (!$this->cloudFetcher->isTableExist($tableName)) {
			throw new DomainException("Table $tableName is not exist.");
		}
		
		$this->scannerDataRepo->setTale($tableName);
		
		$dateFrom = new \DateTimeImmutable($command->dateFrom);
		$dateTo = new \DateTimeImmutable($command->dateTo);
		
		if (!$this->scannerDataRepo->isAllowableAmount($dateFrom, $dateTo)) {
			throw new \DomainException("Query result by this date interval is more allowable amount.");
		}
		
		$scannerDataList = $this->scannerDataRepo->getListWithEventTimeInterval($dateFrom, $dateTo);
		$disconnectScannerDataList = MotionHelper::disconnectStatusFilter($scannerDataList);
		$finedDiving = DrivingHelper::findActiveDrivingFromDisconnect(
			$disconnectScannerDataList,
			$this->scannerDataRepo
		);
		
		$ELDAddress = $this->ELDScannerRepo->getByMACAddress($command->deviceMAC);
		
		$carrierId = $ELDAddress->getCarrierId();
		if (empty($carrierId)) {
			throw new \DomainException("Currier is not exist for this device.");
		}
		$deviceId = $ELDAddress->getId();
		$truckId = null;
		$vin = null;
		//save undefined status
		/** @var $finedDiving ScannerData[] */
		if (!empty($finedDiving)) {
			foreach ($finedDiving as $driving) {
				/** @var ScannerData $scannerDataStart */
				$scannerDataStart = $driving['start'];
				/** @var ScannerData $scannerDataFinish */
				$scannerDataFinish = $driving['finish'];
				
				if (is_null($vin) || $vin !== $scannerDataStart->getVin()) {
					$vin = $scannerDataStart->getVin();
					$truckELDInfo = $this->truckELDInfoRepo->fetchByVinAndCarrierId($vin, $carrierId);
					if (empty($truckELDInfo)) {
						continue;
					}
					$truckId = $truckELDInfo->getEquipmentId();
					if (empty($truckId)) {
						continue;
					}
				}
				
				$dateStart = $scannerDataStart->getDateTimeUTCTime();
				$dateEnd = $scannerDataFinish->getDateTimeUTCTime();
				// create UnassignedDrivingTime
				$unassignedDrivingTime = Ezlogz\Entity\UnassignedDrivingTime::createFromCloud(
					$carrierId,
					$truckId,
					$deviceId,
					$dateStart,
					$dateEnd,
					$scannerDataStart,
					$scannerDataFinish
				);
				
				$this->unassignedDrivingTimeRepo->add($unassignedDrivingTime);
				$this->flusher->flush();
				// all rows by driving period
				$scannerDataListByDriving = $this->scannerDataRepo->getListWithEventTimeInterval($dateStart, $dateEnd);
				// create UnidentifiedPeriodicRecords
				Ezlogz\Entity\UnidentifiedPeriodicRecord::createUnidentifiedPeriodicRecords(
					$unassignedDrivingTime->getId(),
					$scannerDataListByDriving,
					$this->unidentifiedPeriodicRecordRepo
				);
			}
		}
		
		$this->flusher->flush();
		
		return $finedDiving;
	}
}
