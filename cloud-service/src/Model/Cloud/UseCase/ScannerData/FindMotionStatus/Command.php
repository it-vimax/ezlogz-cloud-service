<?php

namespace App\Model\Cloud\UseCase\ScannerData\FindMotionStatus;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $deviceMAC;
	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @App\Validator\CustomDateTime()
	 */
	public $dateFrom;
	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @App\Validator\CustomDateTime()
	 */
	public $dateTo;
}
