<?php

namespace App\Model\Cloud\UseCase\ScannerData\FindDriving;

use App\Helper\CloudHelper;
use App\Helper\DrivingHelper;
use App\Model\Cloud\Entity\ScannerDataRepository;
use App\Model\Ezlogz\Entity\UnidentifiedEngineRecordRepository;
use App\ReadModel\CloudFetcher;
use Laminas\EventManager\Exception\DomainException;

class Handler
{
	private $repo;
	private $enRepo;
	private $cloudFetcher;
	
	public function __construct(ScannerDataRepository $repo, UnidentifiedEngineRecordRepository $enRepo, CloudFetcher $cloudFetcher)
	{
		$this->repo = $repo;
		$this->enRepo = $enRepo;
		$this->cloudFetcher = $cloudFetcher;
	}
	
	public function handle(Command $command)
	{
		$tableName = CloudHelper::generateTableNameByScannerData($command->deviceMAC);
		
		if (!$this->cloudFetcher->isTableExist($tableName)) {
		    throw new DomainException("Table $tableName is not exist.");
		}
		
		$this->repo->setTale($tableName);
		
		if (!$this->repo->isAllowableAmount(new \DateTimeImmutable($command->dateFrom), new \DateTimeImmutable($command->dateTo))) {
			throw new \DomainException("Query result by this date interval is more allowable amount.");
		}
		
		$scannerDatalist = $this->repo->getListWithEventTimeInterval(
				new \DateTimeImmutable($command->dateFrom),
				new \DateTimeImmutable($command->dateTo)
			);
		
		return DrivingHelper::activeDrivingPeriodFilter($scannerDatalist);
	}
}
