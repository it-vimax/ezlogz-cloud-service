<?php

namespace App\Model\Cloud\Entity;

class ScannerCloud
{
	const REASON_TEXT_ON_PERIODIC = 'ON_PERIODIC';
	
	const REASON_TEXT_CONNECT = 'CONN';
	const REASON_TEXT_DISCONNECT = 'DISCONN';
	
	private $formatCheckSum;
	private $serialNumber;
	private $reasonText;
	private $eventUtcTime;
	private $engineOdometer;
	private $engineRPM;
	private $fuelLevel;
	private $batteryVoltage;
	private $j1939EngineHours;
	private $latitudeDegrees;
	private $longitudeDegrees;
	private $odometerMiles;
	private $odometerKilometers;
	private $sealBeltSwitch;
	private $speedKMH;
	private $speedMPH;
	private $vehicleIndicatorNumber;
	
	/**
	 * ScannerCloud constructor.
	 * @param $formatCheckSum
	 * @param $serialNumber
	 * @param $reasonText
	 * @param $eventUtcTime
	 * @param $engineOdometer
	 * @param $engineRPM
	 * @param $fuelLevel
	 * @param $batteryVoltage
	 * @param $j1939EngineHours
	 * @param $latitudeDegrees
	 * @param $longitudeDegrees
	 * @param $odometerMiles
	 * @param $odometerKilometers
	 * @param $sealBeltSwitch
	 * @param $speedKMH
	 * @param $speedMPH
	 * @param $vehicleIndicatorNumber
	 */
	public function __construct(
		$formatCheckSum,
		$serialNumber,
		$reasonText,
		$eventUtcTime,
		$engineOdometer,
		$engineRPM,
		$fuelLevel,
		$batteryVoltage,
		$j1939EngineHours,
		$latitudeDegrees,
		$longitudeDegrees,
		$odometerMiles,
		$odometerKilometers,
		$sealBeltSwitch,
		$speedKMH,
		$speedMPH,
		$vehicleIndicatorNumber
	)
	{
		$this->formatCheckSum = $formatCheckSum;
		$this->serialNumber = $serialNumber;
		$this->reasonText = $reasonText;
		$this->eventUtcTime = $eventUtcTime;
		$this->engineOdometer = $engineOdometer;
		$this->engineRPM = $engineRPM;
		$this->fuelLevel = $fuelLevel;
		$this->batteryVoltage = $batteryVoltage;
		$this->j1939EngineHours = $j1939EngineHours;
		$this->latitudeDegrees = $latitudeDegrees;
		$this->longitudeDegrees = $longitudeDegrees;
		$this->odometerMiles = $odometerMiles;
		$this->odometerKilometers = $odometerKilometers;
		$this->sealBeltSwitch = $sealBeltSwitch;
		$this->speedKMH = $speedKMH;
		$this->speedMPH = $speedMPH;
		$this->vehicleIndicatorNumber = $vehicleIndicatorNumber;
	}
	
	public function toArray(): array
	{
		return [
			'FormatCheckSum' => $this->formatCheckSum,
			'SerialNumber' => $this->serialNumber,
			'ReasonText' => $this->reasonText,
			'EventUtcTime' => $this->eventUtcTime,
			'EngineOdometer' => $this->engineOdometer,
			'EngineRPM' => $this->engineRPM,
			'FuelLevel' => $this->fuelLevel,
			'BatteryVoltage' => $this->batteryVoltage,
			'J1939EngineHours' => $this->j1939EngineHours,
			'LatitudeDegrees' => $this->latitudeDegrees,
			'LongitudeDegrees' => $this->longitudeDegrees,
			'OdometerMiles' => $this->odometerMiles,
			'OdometerKilometers' => $this->odometerKilometers,
			'SealBeltSwitch' => $this->sealBeltSwitch,
			'SpeedKMH' => $this->speedKMH,
			'SpeedMPH' => $this->speedMPH,
			'VehicleIndicatorNumber' => $this->vehicleIndicatorNumber,
		];
	}
	
	public static function fromArray(array $data): self
	{
		return new self(
			$data['FormatCheckSum'],
			$data['SerialNumber'],
			$data['ReasonText'],
			$data['EventUtcTime'],
			$data['EngineOdometer'],
			$data['EngineRPM'],
			$data['FuelLevel'],
			$data['BatteryVoltage'],
			$data['J1939EngineHours'],
			$data['LatitudeDegrees'],
			$data['LongitudeDegrees'],
			$data['OdometerMiles'],
			$data['OdometerKilometers'],
			$data['SealBeltSwitch'],
			$data['SpeedKMH'],
			$data['SpeedMPH'],
			$data['VehicleIndicatorNumber']
		);
	}
	
	/**
	 * @return mixed
	 */
	public function getFormatCheckSum()
	{
		return $this->formatCheckSum;
	}
	
	/**
	 * @return mixed
	 */
	public function getSerialNumber()
	{
		return $this->serialNumber;
	}
	
	/**
	 * @return mixed
	 */
	public function getReasonText()
	{
		return $this->reasonText;
	}
	
	public function RTIsOnPeriodic(): bool
	{
	    return $this->reasonText === self::REASON_TEXT_ON_PERIODIC;
	}
	
	public function isConnect(): bool
	{
	    return $this->reasonText === self::REASON_TEXT_CONNECT;
	}
	
	public function isDisconnect(): bool
	{
	    return $this->reasonText === self::REASON_TEXT_DISCONNECT;
	}
	
	/**
	 * @return mixed
	 */
	public function getEventUtcTime()
	{
		return $this->eventUtcTime;
	}
	
	/**
	 * @return mixed
	 */
	public function getEngineOdometer()
	{
		return $this->engineOdometer;
	}
	
	/**
	 * @return mixed
	 */
	public function getEngineRPM()
	{
		return $this->engineRPM;
	}
	
	/**
	 * @return mixed
	 */
	public function getFuelLevel()
	{
		return $this->fuelLevel;
	}
	
	/**
	 * @return mixed
	 */
	public function getBatteryVoltage()
	{
		return $this->batteryVoltage;
	}
	
	/**
	 * @return mixed
	 */
	public function getJ1939EngineHours()
	{
		return $this->j1939EngineHours;
	}
	
	/**
	 * @return mixed
	 */
	public function getLatitudeDegrees()
	{
		return $this->latitudeDegrees;
	}
	
	/**
	 * @return mixed
	 */
	public function getLongitudeDegrees()
	{
		return $this->longitudeDegrees;
	}
	
	/**
	 * @return mixed
	 */
	public function getOdometerMiles(): float
	{
		return $this->odometerMiles;
	}
	
	/**
	 * @return mixed
	 */
	public function getOdometerKilometers()
	{
		return $this->odometerKilometers;
	}
	
	/**
	 * @return mixed
	 */
	public function getSealBeltSwitch()
	{
		return $this->sealBeltSwitch;
	}
	
	/**
	 * @return mixed
	 */
	public function getSpeedKMH()
	{
		return $this->speedKMH;
	}
	
	/**
	 * @return mixed
	 */
	public function getSpeedMPH()
	{
		return $this->speedMPH;
	}
	
	/**
	 * @return mixed
	 */
	public function getVehicleIndicatorNumber()
	{
		return $this->vehicleIndicatorNumber;
	}
	
	public function getEngineHours(): int
	{
	    return (int) $this->getJ1939EngineHours();
	}
}
