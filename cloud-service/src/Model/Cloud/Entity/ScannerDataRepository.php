<?php

namespace App\Model\Cloud\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class ScannerDataRepository
{
	public const LIMIT = 100000;
	
	/** @var EntityManagerInterface */
	private $em;
	/** @var EntityRepository */
	private $repo;
	
	/**
	 * DrivingRepository constructor.
	 * @param EntityManagerInterface $em
	 * @param EntityRepository $repo
	 */
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->repo = $em->getRepository(ScannerData::class);
	}
	
	public function setTale($tableName): void
	{
		$this->em->getClassMetadata(ScannerData::class)
			->setPrimaryTable(['name' => $tableName]);
		$this->repo = $this->em->getRepository(ScannerData::class);
	}
	
	public function getCountWithEventTimeInterval(\DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo)
	{
		return (int) $this->repo->createQueryBuilder('sd')
			->select('COUNT(sd.id) as count')
			->andWhere('sd.eventUtcTime >= :dateFromTimestamp')
			->andWhere('sd.eventUtcTime <= :dateToTimestamp')
			->setParameter(':dateFromTimestamp', $dateFrom->getTimestamp())
			->setParameter(':dateToTimestamp', $dateTo->getTimestamp())
			->getQuery()->getResult()[0]['count'];
	}
	
	public function isAllowableAmount(\DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo)
	{
		$count = $this->getCountWithEventTimeInterval($dateFrom, $dateTo);
		return $count <= self::LIMIT;
	}
	
	/**
	 * @param \DateTimeImmutable $dateFrom
	 * @param \DateTimeImmutable $dateTo
	 * @return ScannerData[]
	 */
	public function getListWithEventTimeInterval(\DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo)
	{
		return $this->repo->createQueryBuilder('sd')
				->andWhere('sd.eventUtcTime >= :dateFromTimestamp')
				->andWhere('sd.eventUtcTime <= :dateToTimestamp')
				->setParameter(':dateFromTimestamp', $dateFrom->getTimestamp())
				->setParameter(':dateToTimestamp', $dateTo->getTimestamp())
				->getQuery()->getResult();
	}
}
