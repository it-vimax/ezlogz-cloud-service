<?php

namespace App\Model\Cloud\Entity;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class ScannerCloudType extends JsonType
{
	public const NAME = 'scannerData_scannerCloud';
	
	public function convertToDatabaseValue($value, AbstractPlatform $platform)
	{
		/** @var $value ScannerCloud */
	    return $value instanceof ScannerCloud ? $value->toArray() : $value;
	}
	
	public function convertToPHPValue($value, AbstractPlatform $platform)
	{
	    return !empty($value) ? ScannerCloud::fromArray(json_decode($value, true)) : null;
	}
	
	public function getName(): string
	{
	    return self::NAME;
	}
}
