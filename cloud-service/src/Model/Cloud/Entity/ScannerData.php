<?php

namespace App\Model\Cloud\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="null")
 * Class Driving
 * @package App\Model\Cloud\Entity
 */
class ScannerData
{
	public const IGN_ON = 'IGN_ON';
	public const IGN_OFF = 'IGN_OFF';
	
	/**
	 * @var integer
	 * @ORM\Id
	 * @ORM\Column(type="integer", name="id")
	 */
	private $id;
	/**
	 * @var string
	 * @ORM\Column(type="string", name="mac_id")
	 */
	private $macId;
	/**
	 * @var ScannerCloud
	 * @ORM\Column(type="scannerData_scannerCloud", name="json_data")
	 */
	private $cloudData;
	/**
	 * @var integer
	 * @ORM\Column(type="integer", name="event_utc_time")
	 */
	private $eventUtcTime;
	/**
	 * @var string
	 * @ORM\Column(type="string", name="created_at")
	 */
	private $createdAt;
	
	/**
	 * ScannerData constructor.
	 * @param string $macId
	 * @param ScannerCloud $cloudData
	 * @param int $eventUtcTime
	 * @param string $createdAt
	 */
	public function __construct(string $macId, ScannerCloud $cloudData, int $eventUtcTime, string $createdAt)
	{
		$this->macId = $macId;
		$this->cloudData = $cloudData;
		$this->eventUtcTime = $eventUtcTime;
		$this->createdAt = $createdAt;
	}
	
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getMacId(): string
	{
		return $this->macId;
	}
	
	/**
	 * @return ScannerCloud
	 */
	public function getCloudData(): ScannerCloud
	{
		return $this->cloudData;
	}
	
	public function getVin(): string
	{
	    return $this->cloudData->getVehicleIndicatorNumber();
	}
	
	public function getSpeedMPH(): int
	{
		return $this->cloudData->getSpeedMPH();
	}
	
	public function isDeviceConnect(): bool
	{
	    return $this->cloudData->isConnect();
	}
	
	public function isDeviceDisconnect(): bool
	{
	    return $this->cloudData->isDisconnect();
	}
	
	public function isEngineOn(): bool
	{
	    return $this->cloudData->getReasonText() === self::IGN_ON;
	}
	
	public function getEngineHours(): int
	{
	    return $this->cloudData->getEngineHours();
	}
	
	public function getOdomenter(): float
	{
		return $this->cloudData->getOdometerMiles();
	}
	
	public function getLatitudeDegrees(): float
	{
	    return $this->cloudData->getLatitudeDegrees();
	}
	
	public function getLongitudeDegrees(): float
	{
		return $this->cloudData->getLongitudeDegrees();
	}
	
	public function getEngineState()
	{
		return $this->cloudData->getReasonText();
	}
	
	public function showEngineOnOrOffInIntFormat(): int
	{
	    if ($this->isEngineOn()) {
	        return 1;
	    }
	    if ($this->isEngineOff()) {
	        return 0;
	    }
	    
	    throw new \DomainException('Is not validate engine state: ' . $this->getEngineState());
	}
	
	public function isEngineOff(): bool
	{
	    return $this->cloudData->getReasonText() === self::IGN_OFF;
	}
	
	public function MSIsOnPeriodic(): bool
	{
	    return $this->getCloudData()->RTIsOnPeriodic();
	}
	
	public function isDriving(): bool
	{
	    return $this->getSpeedMPH() >= 5;
	}
	
	/**
	 * @return int
	 */
	public function getEventUtcTime(): int
	{
		return $this->eventUtcTime;
	}
	
	public function getDateTimeUTCTime(): DateTimeImmutable
	{
	    return new \DateTimeImmutable(date('Y-m-d H:i:s', $this->eventUtcTime));
	}
	
	/**
	 * @return string
	 */
	public function getCreatedAt(): string
	{
		return $this->createdAt;
	}
}
