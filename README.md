## Deploy
1. clone repository
2. copy ```cloud-service/.env``` to ```cloud-service/.env.local```
3. send config to db (CLOUD_DATABASE_URL, EZLOGZ_DATABASE_URL) in .env.local file
4. run ```composer update``` in cloud-service folder
5. open ApiDoc.txt file for read api documentation

If you want to deploy Production environment, you need change APP_ENV in .env file from ```dev``` to ```prod``` value. 